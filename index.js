// A "closer to real-life" app example
// using 3rd party middleware modules
// P.S. MWs calls be refactored in many files

// long stack trace (+clarify from co) if needed
if (process.env.TRACE) {
  require('./libs/trace');
}

var koa = require('koa');
var app = koa();

var config = require('config');

var history = [];

// keys for in-koa KeyGrip cookie signing (used in session, maybe other modules)
app.keys = [config.secret];

var path = require('path');
var fs = require('fs');
var middlewares = fs.readdirSync(path.join(__dirname, 'middlewares')).sort();

middlewares.forEach(function(middleware) {
  app.use(require('./middlewares/' + middleware));
});

// ---------------------------------------

// can be split into files too
var Router = require('koa-router');

var router = new Router();

//Стартовая страница
router.get('/subscribe', function* (next) {

  yield this.render('index', {
    content: ''
  });

});

//Запрос на публикацию нового (если есть) и выдачу истории сообщений
router.get('/publish', function* (next) {

  var message = this.request.query.message;
  console.log("publish %s", message);

  if (message) history.push(message);

  this.body = history.join('<->');
});

//идем на Стартовую страницу
router.get('/', function*() {
  this.redirect('/subscribe');
});

app.use(router.routes());

app.listen(3000);
