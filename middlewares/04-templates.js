// initialize template system early, to let error handler use them
// koa-views is a wrapper around many template systems!
// most of time it's better to use the chosen template system directly

var jade = require('koa-jade-render');
var config = require('config');
var path = require('path');

module.exports = jade(path.join(config.root, 'templates'));


